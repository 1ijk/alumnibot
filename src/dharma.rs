use std::collections::HashMap;
use slack;
use redis::Connection;
use slack::{Event,RtmClient,Message};
use crate::{Wheel,Soul};

pub struct Dharma {
    redis: &'static mut Connection,
    wheel: Wheel,
    souls: HashMap<String, Soul>,
}

struct Judgement {
    channel: String,
    message: String,
}

impl Dharma {
    pub fn new(redis: &'static mut Connection) -> Self {
        Dharma{
            redis: redis,
            wheel: Wheel::new(),
            souls: HashMap::new(),
        }
    }

    fn judge(&mut self, message: Message) -> Option<Judgement> {
        match message {
            Message::Standard(message) => {
                let channel = message.channel?;

                match message.text {
                    Some(text) => {
                        self.wheel.turn(text.as_str());
                        let message = format!("{:?}", self.wheel.eye);
                        Some(Judgement{
                            channel: channel,
                            message: message,
                        })
                    },
                    None => None,
                }
            },
            _ => None,
        }
    }
}

impl slack::EventHandler for Dharma {
    fn on_event(&mut self, cli: &RtmClient, event: Event) {
        match event {
            Event::Message(message) => {
                let message = *message;
                match self.judge(message) {
                    Some(judgement) => {
                        let _ = cli.sender()
                            .send_message(judgement.channel.as_str(), judgement.message.as_str());
                    },
                    None => ()
                }
            },
            _ => ()
        }
    }

    fn on_close(&mut self, _cli: &RtmClient) {
        self.souls.iter().map(|(_, soul)| soul.save_to_redis(self.redis) );
        println!("bye!");
    }

    fn on_connect(&mut self, _cli: &RtmClient) {
        println!("hello!");
    }
}