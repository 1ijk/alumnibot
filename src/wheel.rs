use std::str::FromStr;

use crate::{Karma, Deed};

pub struct Wheel {
    pub eye: Karma
}

impl Wheel {
    pub fn new() -> Wheel {
        Wheel{
            eye: Karma{
                username: String::from("nobody"),
                operator: Deed::Neutral,
                rewarder: String::from("Polyphemus"),
                reason: String::from("naught")
            }
        }
    }

    pub fn turn(&mut self, message: &str) {
        match Karma::from_str(message) {
            Ok(karma) => {
                match (karma.username.as_str(), karma.reason.as_str()) {
                    // just a plus or minus
                    ("", "") => {
                        self.eye.operator = karma.operator;
                    },
                    // a plus or minus with possibly new reason
                    ("", reason) => {
                       self.eye.reason = String::from(reason);
                       self.eye.operator = karma.operator;
                   },
                   // possibly new user and new reason
                   (_, _) => {
                       self.eye = karma;
                   }
                }
            },
            Err(_) => {}
        }
    }
}