mod karma;
mod wheel;
mod dharma;
mod soul;

use karma::{Karma,Deed};
use wheel::Wheel;
use dharma::Dharma;
use slack::RtmClient;
use redis::Client;
use soul::Soul;

fn main() {
    let mut dharma = match Client::open("redis://127.0.0.1:6379/") {
        Ok(client) => match client.get_connection() {
            Ok(conn) => Dharma::new(conn),
            Err(err) => panic!(err)
        },
        Err(err) => panic!(err)
    };

    let args: Vec<String> = std::env::args().collect();
    let api_key = match args.len() {
        0 | 1 => {
            panic!("No api-key in args! Usage: cargo run -- <api-key>")
        }
        x => args[x - 1].clone(),
    };

    let r = RtmClient::login_and_run(&api_key, &mut dharma);
    match r {
        Ok(_) => {}
        Err(err) => println!("Error: {}", err),
    }
}