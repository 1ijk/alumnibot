use std::collections::HashMap;
use slack::User;
use redis::{Connection, cmd,RedisError};

pub struct Soul {
    bandha: HashMap<String, i64>,
    user_id: String,
}

impl Soul {
    pub fn load_from_redis(user: User, conn: &mut Connection) -> Option<Self> {
        match user.id {
            Some(user_id) => Some(Self{
                user_id: user_id.clone(),
                bandha: Self::fetch(user_id.clone(), conn)
            }),
            None => None
        }
    }

    pub fn save_to_redis(&self, conn: &mut Connection) -> Result<(), RedisError> {
        let mut data = Vec::new();

        for (key, value) in self.bandha.iter() {
            data.push(vec!(key.clone(), format!("{}", value)))
        }

        cmd("HSET")
            .arg(self.user_id.clone())
            .arg(data)
            .query(conn)
    }

    fn fetch(user_id: String, conn: &mut Connection) -> HashMap<String, i64> {
        let data = cmd("HGETALL")
            .arg(user_id)
            .query(conn);

        match data {
            Ok(data) => data,
            Err(_) => HashMap::new()
        }
    }

    pub fn new(user : User) -> Option<Self> {
        let fresh = HashMap::new();

        Some(Self {
            bandha: fresh,
            user_id: user.id?,
        })
    }
}