use std::str::FromStr;
use std::fmt;
use regex::{Regex};

#[derive(Clone, Debug, PartialEq)]
pub enum Deed {
    Neutral,
    Good,
    Bad,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Karma {
    pub rewarder: String,
    pub username: String,
    pub operator: Deed,
    pub reason: String,
}

impl<'a> fmt::Display for Karma {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let res = match self.operator {
            Deed::Good => write!(f, "{} gave {} karma", self.rewarder, self.username),
            Deed::Bad => write!(f, "{} took karma from {}", self.rewarder, self.username),
            _ => write!(f, "{} recognizes {}", self.rewarder, self.username)
        };

        match self.reason.as_str() {
            "" => res,
            reason => write!(f, " for {}", reason)
        }
    }
}

impl FromStr for Karma {
    type Err = String;

    fn from_str(message: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"^(.*)?(\+\+|--)(?:\s+(?:for)\s+(.+))?").unwrap();

        let karma = match re.captures(message) {
            Some(data) => {
                let username = data.get(1).map_or("", |m| m.as_str());
                let operator = match data.get(2).map_or("", |m| m.as_str()) {
                    "++" => Deed::Good,
                    "--" => Deed::Bad,
                    _ => Deed::Neutral,
                };

                let reason = data.get(3).map_or("", |m| m.as_str());

                Ok(Karma{
                    rewarder: String::from(""),
                    username: String::from(username),
                    operator: operator,
                    reason: String::from(reason)
                })
            }
            None => Ok(
                Karma{
                    rewarder: String::from("Polyphemus"),
                    username: String::from("nobody"),
                    operator: Deed::Neutral,
                    reason: String::from("nothing"),
                }
            )
        };

        karma
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_karma_from_str() {
        assert_eq!(
        Karma::from_str(&r"kennabot++ for karma").unwrap(),
        Karma{
            username: String::from("kennabot"),
            operator: Deed::Good,
            reason: String::from("karma"),
            rewarder: String::from("")
            }
        );

        assert_eq!(
        Karma::from_str(&r"kennabot++").unwrap(),
        Karma{
            username: String::from("kennabot"),
            operator: Deed::Good,
            reason: String::from(""),
            rewarder: String::from("")
            }
        );

                assert_eq!(
        Karma::from_str(&r"kennabot--").unwrap(),
        Karma{
            username: String::from("kennabot"),
            operator: Deed::Bad,
            reason: String::from(""),
            rewarder: String::from("")
            }
        );

                        assert_eq!(
        Karma::from_str(&r"--").unwrap(),
        Karma{
            username: String::from(""),
            operator: Deed::Bad,
            reason: String::from(""),
            rewarder: String::from("")
            }
        );

                                assert_eq!(
        Karma::from_str(&r"++").unwrap(),
        Karma{
            username: String::from(""),
            operator: Deed::Good,
            reason: String::from(""),
            rewarder: String::from("")
            }
        );

                                        assert_eq!(
        Karma::from_str(&r"totally irrelevant conversation").unwrap(),
        Karma{
            username: String::from("nobody"),
            operator: Deed::Neutral,
            reason: String::from("nothing"),
            rewarder: String::from("Polyphemus")
            }
        );
    }
}